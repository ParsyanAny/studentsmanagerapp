﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StudentsManagerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var stList = StudentListCreater.CreateList(55);

            var sortedStudents = stList.OrderBy(x=>x.Grade).GroupBy(x => x.University);
            #region
            //var sortedStudents = from s in (from s in stList                              
            //             orderby s.Grade
            //             select s)
            //             group s by s.University;
            #endregion
            Paint();
            Print(sortedStudents);
            Console.Read();
        }
        static public void Print(IEnumerable<IGrouping<University, Student>> stList)
        {
            Random rand = new Random();
            foreach (var item in stList)
            {
            Console.ForegroundColor = ConsoleColor.DarkGray;
                foreach(var i in item)
                {
                Console.WriteLine($"{i.University}\t\t{i.Grade}\t{i.Fullname}     \t{i.Age} \t{i.Email}    \t {i.FullNumber}   |");
                }
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(new string('_',110) + "|");
            }
        }
        static public void Paint()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("University    Grade       Fullname\t\tAge   \t \tEMail \t\t     \t   PhoneNumber");
            Console.WriteLine(new string('_',110));

            Console.ResetColor();
            
        }
    }
}
